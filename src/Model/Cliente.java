package Model;

import DAO.ConnectionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Cliente  extends Pessoa{
    private int idCliente;
    private double saldo;
    private double salario;
    private double limite;
    
    private Connection conn = null;
    private ResultSet resul;

    public Cliente() {
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

   
    public Cliente selectClienteById(int id){
        conn = ConnectionDB.getConnection();
        PreparedStatement stmt = null;
        Cliente cli = new Cliente();
        try {
            
            stmt = conn.prepareStatement("SELECT idPessoa,nome,cpf,endereco,numeroEndereco, cep, email, idCliente, saldo, salario, limite, senha, usuario, usuariosistema.tipo AS tipoUsuario, idUsuario FROM `usuariosistema`,(SELECT * from pessoas,clientes WHERE clientes.fkPessoa = idPessoa) AS CLIENTE WHERE usuariosistema.fkPessoa = idPessoa AND usuariosistema.status = 'A' AND  idUsuario = ?");            
            stmt.setInt(1, id);
            
            resul = stmt.executeQuery();
            
            while (resul.next()) {
                cli.setIdPessoa(resul.getInt("idPessoa"));
                cli.setNome(resul.getString("nome"));
                cli.setCpf(resul.getString("cpf"));
                cli.setEndereco(resul.getString("endereco"));
                cli.setNumeroEndereco(resul.getInt("numeroEndereco"));
                cli.setCep(resul.getString("cep"));
                cli.setEmail(resul.getString("email"));
                cli.setIdCliente(resul.getInt("idCliente"));
                cli.setSaldo(resul.getDouble("saldo"));
                cli.setSalario(resul.getDouble("salario"));                
                cli.setLimite(resul.getDouble("limite"));                
                cli.setSenha(resul.getString("senha"));                
                cli.setUsuario(resul.getString("usuario"));             
                cli.setTipo(resul.getString("tipoUsuario"));
                cli.setIdUsuario(resul.getInt("idUsuario"));
            }
            
            return cli;
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO AO TENTAR BUSCAR CLIENTE NO SISTEMA: Verifique Com o Administrador !!"+ex);
            return null;
        } 
        finally{
            ConnectionDB.closeConnection(conn, stmt);
        }
    }
    
    
    
}

package Model;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Extrato {
    private int id;
    private double valor;
    private String data;
    private String tipo;

    public Extrato() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getData() {
        return data;
    }

    public void setData(Date data) {
        String dataFormatada = new SimpleDateFormat("dd / MM / yyyy").format(data);
        this.data = dataFormatada;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        if(tipo.equals("D")){
            this.tipo = "DEPOSITO";
        }
        else if(tipo.equals("S")){
            this.tipo = "SAQUE"; 
        }
    }
}

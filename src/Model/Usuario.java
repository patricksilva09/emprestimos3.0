package Model;

import DAO.ConnectionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;


public class Usuario{
    private int IdUsuario;
    private String senha;
    private String usuario;
    private String status;

    private Connection conn = null;
    private ResultSet resul;
    
    public Usuario() {
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int selectUsuario(String usuario, String senha){
        
        this.conn = ConnectionDB.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement("SELECT fkPessoa FROM usuarioSistema WHERE usuario = ? and senha = ? AND status =  'A'");
            stmt.setString(1, usuario);
            stmt.setString(2, senha);
         
            this.resul = stmt.executeQuery();          

            if (resul.next()) {
                return resul.getInt("fkPessoa");
            }
            else{
                return 0;
            }
      
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro Ao -TENTAR FAZER LOGIN NO SISTEMA- Verifique Com o Administrador !!" +ex);
            return 0;
        } 
        finally{
            ConnectionDB.closeConnection(conn, stmt);
        }
 
    }
          
}

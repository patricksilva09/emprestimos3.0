package Model;

import DAO.ConnectionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Pessoa extends Usuario {
    private int idPessoa;
    private String nome;
    private String cpf;
    private String endereco;
    private int numeroEndereco;
    private String cep;
    private String email;
    private String tipo;
    
    private Connection conn = null;
    private ResultSet resul;

    public Pessoa() {
    }

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(int numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public Cliente selectPessoaById(int id){
        conn = ConnectionDB.getConnection();
        PreparedStatement stmt = null;
        
        Cliente cli = new Cliente();
        try {
            
            stmt = conn.prepareStatement("SELECT idPessoa,nome,cpf,endereco,numeroEndereco, cep, email, idCliente, saldo, salario, limite, senha, usuario, usuariosistema.tipo AS tipoUsuario, idUsuario FROM `usuariosistema`,(SELECT * from pessoas,clientes WHERE clientes.fkPessoa = idPessoa) AS CLIENTE WHERE usuariosistema.fkPessoa = idPessoa AND usuariosistema.status = 'A' AND  idUsuario = ?");            
            stmt.setInt(1, id);
            
            resul = stmt.executeQuery();
            
            while (resul.next()) {
                cli.setIdPessoa(resul.getInt("idPessoa"));
                cli.setNome(resul.getString("nome"));
                cli.setCpf(resul.getString("cpf"));
                cli.setEndereco(resul.getString("endereco"));
                cli.setNumeroEndereco(resul.getInt("numeroEndereco"));
                cli.setCep(resul.getString("cep"));
                cli.setEmail(resul.getString("email"));
                cli.setIdCliente(resul.getInt("idCliente"));
                cli.setSaldo(resul.getDouble("saldo"));
                cli.setSalario(resul.getDouble("salario"));                
                cli.setLimite(resul.getDouble("limite"));                
                cli.setSenha(resul.getString("senha"));                
                cli.setUsuario(resul.getString("usuario"));             
                cli.setTipo(resul.getString("tipoUsuario"));
                cli.setIdUsuario(resul.getInt("idUsuario"));
            }
            
            return cli;
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO AO TENTAR BUSCAR CLIENTE NO SISTEMA: Verifique Com o Administrador !!"+ex);
            return null;
        } 
        finally{
            ConnectionDB.closeConnection(conn, stmt);
        }
    }
    
    
    
}

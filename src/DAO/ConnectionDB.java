package DAO;

// IMPORTANDO 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDB {
    
    private static final String DRIVER   = "com.mysql.jdbc.Driver";
    private static final String USERDB   = "root";
    private static final String PASSWORD = "";
    private static final String IPSERVER = "127.0.0.1";
    private static final String DATABASE = "emprestimos3.0";
    
    private static final String URLCONNECTION = "jdbc:mysql://"+IPSERVER+"/"+DATABASE;
    
    private static Connection conn;
    
    public static Connection getConnection(){
        
        try {
            Class.forName(DRIVER);
            
            conn = DriverManager.getConnection(URLCONNECTION, USERDB, PASSWORD);
            
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("ERRO AO TENTAR ENCONTRAR CLASS DE CONEXAO", e);
        } catch(SQLException e){
            throw new RuntimeException("== ERRO AO TENTAR CONCECTAR AO BANCO ==", e);
        } 
        
        return conn;
    }
   
    public static void closeConnection(Connection con){
        try {
            if(con != null){
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void closeConnection(Connection con, PreparedStatement stmt){
        closeConnection(con);
        
        try {
            if(stmt != null){
                stmt.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs){
        closeConnection(con, stmt);
        
        try {
            if(rs != null){
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

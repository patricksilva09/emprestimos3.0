package DAO;

import Model.Extrato;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class ExtratoDAO {
    
    private Connection conn = null;

    public ExtratoDAO() {
        
    }

    public List<Extrato> selectExtratoByIdPessoa(int id){
        conn = ConnectionDB.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Extrato> extratos = new ArrayList<>();
        
        try {
            
            stmt = conn.prepareStatement("SELECT * FROM `transacoesbancarias` WHERE fkPessoa = ?");            
            stmt.setInt(1, id);
            
            rs = stmt.executeQuery();
            
            while (rs.next()) {                
              Extrato extrato = new Extrato();
              extrato.setId(rs.getInt("idTransacao"));
              extrato.setValor(rs.getDouble("valor"));
              extrato.setData(rs.getDate("dataTransacao"));
              extrato.setTipo(rs.getString("tipo"));
              
              extratos.add(extrato);
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO AO TENTAR BUSCAR EXTRATO DO CLIENTE: Verifique Com o Administrador !!"+ex);
        }finally{
            ConnectionDB.closeConnection(conn, stmt, rs);
        }
      
        
        return extratos;
    }
    
    
    
    
    
}

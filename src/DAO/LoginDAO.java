package DAO;


import Model.Cliente;
import Model.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class LoginDAO {

    private Connection conn = null;
    private ResultSet resul;
    private Cliente cliente = null;
    
    
    
    public LoginDAO(){
    }
    
    public Pessoa logar(String usuario,  String senha){
        this.conn = ConnectionDB.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = conn.prepareStatement("SELECT fkPessoa,tipo FROM usuarioSistema WHERE usuario = ? and senha = ? AND status =  'A'");
            stmt.setString(1, usuario);
            stmt.setString(2, senha);
         
            this.resul = stmt.executeQuery();          

            if (resul.next()) {
                
                if(this.resul.getString("tipo").equals("C")){
                    this.cliente  = new Cliente().selectClienteById(this.resul.getInt("fkPessoa"));
                }
                else if(this.resul.getString("tipo").equals("F")){
                    JOptionPane.showMessageDialog(null, "Usuario Pessoa ID:"+this.resul.getInt("fkPessoa")+" Tipo Pessoa: Funcionario");
                    return null;
                }
            }
            else{
                return null;
            }
      
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro Ao -TENTAR FAZER LOGIN NO SISTEMA- Verifique Com o Administrador !!" +ex);
            return null;
        } 
        finally{
            ConnectionDB.closeConnection(conn, stmt);
        }
        
        return this.cliente;
    }
    
    
    
}

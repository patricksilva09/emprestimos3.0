package controller;

import DAO.LoginDAO;
import Model.Pessoa;
import Model.Usuario;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class LoginController {

    private Usuario usr = new Usuario();
    private Pessoa resposta;

    public LoginController() {

    }

    public Pessoa efeturarLogin(String usuario, String senha) {
      
        if (usuario.equals("")) {
            JOptionPane.showMessageDialog(null, "Campo Usuario Vazio Preencha.");
        } else if (senha.equals("")) {
            JOptionPane.showMessageDialog(null, "Campo Senha Vazio Preencha.");
        } else {
            this.resposta = new LoginDAO().logar(usuario, senha);
            
            if(resposta != null){
               return this.resposta;
            }else{
               JOptionPane.showMessageDialog(null, "Usuario ou Senha Não Encontrados. Verifique Os Dados!!!");
            }
        }
       return null;
    }

}
